$(document).ready(function () {
	"use strict";

	/* ------------- TEXT ANIMATION PART ---------------*/
	//new WOW().init();
	var wow_animations = new WOW({
		boxClass: 'wow',
		animateClass: 'animated'
	});
	wow_animations.init();

	/* ------------- OWL CAROUSEL SYNTAX ---------------*/

	//hero banner Slider
	var hero_slider_length = $("#hero_slider_carousel").find('.item').length;
	//alert(hero_slider_length);		
	var owl = $("#hero_slider_carousel").owlCarousel({
		dots: hero_slider_length > 1 ? true : false,
		nav: hero_slider_length > 1 ? false : false,
		touchDrag: hero_slider_length > 1 ? true : false,
		mouseDrag: hero_slider_length > 1 ? true : false,
		loop: hero_slider_length > 1 ? true : false,
		autoplay: hero_slider_length > 1 ? 3000 : false,
		autoplayHoverPause: hero_slider_length > 1 ? true : false,
		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 1
			},
			768: {
				items: 1
			},
			1000: {
				items: 1
			}
		}
	});

	//onLoad image resize, to take scrollbar width
	$(window).on('load', function () {
		owl.trigger('refresh.owl.carousel');
	});

	// List items Slider	
	var list_items_length = $("#list_items_carousel").find('.item').length;
	//alert(list_items_length);
	$("#list_items_carousel").owlCarousel({
		//autoplay: eq_height > 1 ? 3000 : false,
		//autoplayHoverPause: list_items_length > 1 ? true : false,
		margin: 14,
		responsive: {
			0: {
				items: 1,
				dots: list_items_length > 1 ? false : false,
				touchDrag: list_items_length > 1 ? true : false,
				mouseDrag: list_items_length > 1 ? true : false,
				loop: list_items_length > 1 ? true : false,
				nav: list_items_length > 1 ? true : false
			},
			600: {
				items: 2,
				dots: list_items_length > 2 ? false : false,
				touchDrag: list_items_length > 2 ? true : false,
				mouseDrag: list_items_length > 2 ? true : false,
				loop: list_items_length > 2 ? true : false,
				nav: list_items_length > 2 ? true : false
			},
			768: {
				items: 2,
				dots: list_items_length > 2 ? false : false,
				touchDrag: list_items_length > 2 ? true : false,
				mouseDrag: list_items_length > 2 ? true : false,
				loop: list_items_length > 2 ? true : false,
				nav: list_items_length > 2 ? true : false
			},
			1000: {
				items: 4,
				dots: list_items_length > 4 ? false : false,
				touchDrag: list_items_length > 4 ? true : false,
				mouseDrag: list_items_length > 4 ? true : false,
				loop: list_items_length > 4 ? true : false,
				nav: list_items_length > 4 ? true : false
			}
		}
	});


	// Testimonial Slider
	var testimonial_slider_length = $(".testimonial_slider").find('.item').length;
	//alert(testimonial_slider_length);		
	$(".testimonial_slider").owlCarousel({
		dots: testimonial_slider_length > 1 ? false : false,
		nav: testimonial_slider_length > 1 ? true : false,
		touchDrag: testimonial_slider_length > 1 ? true : false,
		mouseDrag: testimonial_slider_length > 1 ? true : false,
		loop: testimonial_slider_length > 1 ? true : false,
		autoplay: testimonial_slider_length > 1 ? 3000 : false,
		autoplayHoverPause: testimonial_slider_length > 1 ? true : false,
		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 1
			},
			768: {
				items: 1
			},
			1000: {
				items: 1
			}
		}
	});

	// Latest infosys
	var latest_infosys_length = $("#latest_infosys").find('.item').length;
	//alert(testimonial_slider_length);		
	$("#latest_infosys").owlCarousel({
		dots: latest_infosys_length > 1 ? false : false,
		nav: latest_infosys_length > 1 ? true : false,
		touchDrag: latest_infosys_length > 1 ? true : false,
		mouseDrag: latest_infosys_length > 1 ? true : false,
		loop: latest_infosys_length > 1 ? true : false,
		autoplay: latest_infosys_length > 1 ? 3000 : false,
		autoplayHoverPause: latest_infosys_length > 1 ? true : false,
		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 1
			},
			768: {
				items: 1
			},
			1000: {
				items: 1
			}
		}
	});

	// Testimonial Video
	var testimonial_slider_video = $(".testimonial_video").find('.item').length;
	//alert(testimonial_slider_video);		
	$(".testimonial_video").owlCarousel({
		slideBy: 2,
		margin: 20,
		dots: testimonial_slider_video > 2 ? false : false,
		nav: testimonial_slider_video > 2 ? true : false,
		touchDrag: testimonial_slider_video > 2 ? true : false,
		mouseDrag: testimonial_slider_video > 2 ? true : false,
		loop: testimonial_slider_video > 2 ? true : false,
		autoplay: testimonial_slider_video > 2 ? 3000 : false,
		autoplayHoverPause: testimonial_slider_video > 2 ? true : false,
		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 1
			},
			768: {
				items: 2
			},
			1000: {
				items: 2
			}
		}
	});

	//hero banner Slider
	var ppl_slider_carousel_length = $("#ppl_slider_carousel").find('.item').length;
	//alert(hero_slider_length);		
	$("#ppl_slider_carousel").owlCarousel({
		dots: ppl_slider_carousel_length > 1 ? true : false,
		nav: ppl_slider_carousel_length > 1 ? false : false,
		touchDrag: ppl_slider_carousel_length > 1 ? true : false,
		mouseDrag: ppl_slider_carousel_length > 1 ? true : false,
		loop: ppl_slider_carousel_length > 1 ? true : false,
		autoplay: ppl_slider_carousel_length > 1 ? 3000 : false,
		autoplayHoverPause: ppl_slider_carousel_length > 1 ? true : false,
		responsive: {
			0: {
				items: 1
			},
			600: {
				items: 1
			},
			768: {
				items: 1
			},
			1000: {
				items: 1
			}
		}
	});
	/* ------------- Path Name checking for header ---------------*/
	var pathName = window.location.pathname.toLowerCase();
	var pageUrl = 'others';
	if (pathName === '/careers/pages/index.aspx' || pathName === '/careers/' || pathName === '/careers' || pathName === '/jp/careers/pages/index.aspx' || pathName === '/jp/careers/' || pathName === '/jp/careers') {
		pageUrl = 'home';
	}
	/* ------------- Scroll Fixed second Header Industries ---------------*/
	var num = $(this).scrollTop() !== 0; /*$('header').offset().top;*/
	$(window).bind('scroll', function () {
		if (pageUrl !== 'home') {
			if ($(window).scrollTop() > num) {
				$('.hero-list').addClass('hero-list1');
				$('.listmenu').css('z-index', '9999');
				$('.scrollbg-show').addClass('show-strip');
				$('#logo').attr('fill', '#007cc3');
				$('.menu-bg').addClass('reverseMenu');
				$('.burger > .icon-bar1').addClass('icon-bar11');
				$('.burger > .icon-bar2').addClass('icon-bar21');
				$('.burger > .icon-bar3').addClass('icon-bar31');
				$('.hidden-list').addClass('visible-list');
				$('.menu-bg, .burger').css('margin-top', '12px');
				$('.hamburger-menu > .search__color').find('.search-icon').css('top', '20px');
				$('.hamburger-menu > .search__color').find('.btn1').css('color', '#000');
			} else {
				num = $(this).scrollTop() !== 0;
				$('.hero-list').removeClass('hero-list1');
				$('.listmenu').css('z-index', '2');
				$('.scrollbg-show').removeClass('show-strip');
				$('#logo').attr('fill', '#fff');
				$('.menu-bg').removeClass('reverseMenu');
				$('.burger > .icon-bar1').removeClass('icon-bar11');
				$('.burger > .icon-bar2').removeClass('icon-bar21');
				$('.burger > .icon-bar3').removeClass('icon-bar31');
				$('.hidden-list').removeClass('visible-list');
				$('.menu-bg, .burger').css('margin-top', '18px');
				$('.hamburger-menu > .search__color').find('.search-icon').css('top', '27px');
				$('.hamburger-menu > .search__color').find('.btn1').css('color', '#fff');
			}
		}
	});

	/* enable scrollify based on screenwidth */
	function enableScrollify() {

		/* scrollify effect */
		$.scrollify({
			section: ".scroll-section",
			sectionName: false,
			interstitialSection: ".scroll-height",
			easing: "easeOutExpo",
			scrollSpeed: 900,
			offset: 0,
			scrollbars: true,
			standardScrollElements: ".scroll-standard",
			setHeights: false,
			overflowScroll: true,
			updateHash: false,
			touchScroll: true,
			before: function (index) {
				if (index > 3) {
					//$('.scroll-section').removeClass('fromUp');
				} else {
					/*if (!$.scrollify.current().hasClass('fromUp')) {
						$('.scroll-section').removeClass('fromUp');
						$.scrollify.current().addClass('fromUp');
					}*/
				}
				/*				var player;
								if (index === 0) {
									if (player !== undefined) {
										player.playVideo();
									}
								} else {
									if (player !== undefined) {
										player.pauseVideo();
									}
								}*/

				$('.animateFirst').removeClass('fadeInLeft');
				$('.animateSecond').removeClass('fadeInRight');
				$('.animateThird').removeClass('fadeInUp');
				$('.animateFourth').removeClass('fadeInDown');
				$.scrollify.current().find('.animateFirst').addClass('fadeInLeft');
				$.scrollify.current().find('.animateSecond').addClass('fadeInRight');
				$.scrollify.current().find('.animateThird').addClass('fadeInUp');
				$.scrollify.current().find('.animateFourth').addClass('fadeInDown');
			},
			after: function () {},
			afterResize: function () {
				$.scrollify.update();
			},
			afterRender: function () {}
		});
	}


	/*************** Index start ***************/
	//home&end key press event
	if (pageUrl === 'home') {
		document.addEventListener('keydown', function (event) {
			if (event.keyCode === 36) {
				$.scrollify.move('#1');
			}
			if (event.keyCode === 35) {
				if ($.scrollify.current().attr('id') !== 'day_infosys') {
					$.scrollify.instantMove('#5');
				}
			}
		}, true);

		/* get screen width */
		var screenWidth = $(window).width();
		//var loopStart = 0;

		$(window).on('load', function () {

			if (screenWidth > 1024) {
				enableScrollify();
			} else {
				var herovide = document.getElementById('herovideo');
				herovide.autoplay = false;
				herovide.load();
			}

		});

		/* based on screen width enable/disable scrollify & tickering */
		if (screenWidth > 1024) {
			enableScrollify();
			//loopStart = 4;

			enableTickering();
		} else {
			$('.beMoreSubText').css('visibility', 'visible');
			$('.addWowAnimation').addClass('wow fadeInUp');
		}

		$(window).resize(function () {
			screenWidth = $(window).width();
			if (screenWidth > 1024) {
				enableScrollify();
				//loopStart = 4;

				//enable or disable tickering
				enableTickering();
			} else {
				$.scrollify.disable();
				$('body').removeAttr('style');
				//loopStart = 0;

				//enable or disable tickering
				$('.beMoreSubText').css('visibility', 'visible');
				$('.blinkTxt').css('display', 'none');
				$('.typed-cursor').css('display', 'none');
			}
		});

		/* This part handles the highlighting functionality of left nav. */
		var aChildren = $("nav.sticky-left-nav li").children(); // find the a children of the list items
		var aArray = []; // create the empty aArray
		for (var i = 0; i < aChildren.length; i++) {
			var aChild = aChildren[i];
			var ahref = $(aChild).attr('href');
			aArray.push(ahref);
		}
		$(window).scroll(function () {
			var windowPos = $(window).scrollTop(); // get the offset of the window from the top of page
			var windowHeight = $(window).height(); // get the height of the window
			var windowBottom = windowPos + windowHeight; //calculate the bottom of the window
			var docHeight = $(document).height();
			//change j value based on no. of full scroll sections
			for (var j = 0; j < aArray.length; j++) {
				var theID = aArray[j];
				var divPos = $(theID).offset().top; // get the offset of the div from the top of page
				var divHeight = $(theID).height(); // get the height of the div in question			
				var divBottom = divPos + divHeight; // calculate the bottom of the div
				//if (windowPos >= divPos && windowPos < (divPos + divHeight)) {
				if (((windowPos <= divPos) && (windowBottom >= divBottom)) || ((windowPos > divPos) && (windowBottom >= divBottom) && (windowPos < divBottom)) || ((windowPos <= divPos) && (windowBottom < divBottom) && (windowBottom > divPos)) || ((windowPos > divPos) && (windowBottom < divBottom))) {
					$('.sticky-left-nav').find('li').removeClass('nav-active mb5');
					$("a[href='" + theID + "']").closest('li').addClass("nav-active mb5");
					//if (loopStart === 0) {
					//if (() && (!$('nav.sticky-left-nav').find('li').hasClass('darkText'))) {
					screenWidth = $(window).width();
					if (screenWidth > 1024) {
						if (j > 3) { //alert(j);
							$('nav.sticky-left-nav').find('li').addClass('darkText');
							$('body').removeAttr('style');
							//$.scrollify.disable();
						} else if (j < 4) {
							$('nav.sticky-left-nav').find('li').removeClass('darkText');
							$('body').css('overflow', 'hidden');
							//$.scrollify.enable();							

							//$.scrollify.update();

							//enableScrollify();
							//loopStart = 4;

						}

					}
					//}
				}
			}

			/* change navigation text color when position reaches bottom */
			if (windowPos + windowHeight === docHeight) {
				$('nav.sticky-left-nav').find('li').addClass('darkText');
				$('.sticky-left-nav').find('li').removeClass('nav-active mb5');
				$('.sticky-left-nav').find('li:last-child()').addClass('nav-active mb5');
			}
			/* change navigation text color when position reaches top */
			if (windowPos === 0) {
				$('nav.sticky-left-nav').find('li').removeClass('darkText');
				$('.sticky-left-nav').find('li').removeClass('nav-active mb5');
				$('.sticky-left-nav').find('li:first-child()').addClass('nav-active mb5');
			}

			/* check footer offset for left sticky nav */
			checkFooterOffset();

			if ($(this).scrollTop() !== 0) {
				$('.header-menu').fadeOut(700);
				$('.search__color').fadeOut(700);
			} else {
				$('.header-menu').fadeIn(700);
				$('.search__color').fadeIn(700);
			}
		});
	}


	// onload Menu Active
	if ($(window).scrollTop() > 100) {
		var txx = $(window).scrollTop() + 1;
		window.scrollTo(0, txx);
	}
	if (pageUrl === 'home') {
		// onload darkText, scroll, hidden add and remove
		screenWidth = $(window).width();
		if (screenWidth > 1024) {
			if ($(window).scrollTop() < $('#day_infosys').offset().top) {
				//$(".scrollbg-show").addClass("home-strip");
				$('body').css('overflow', 'hidden');
				$('.sticky-left-nav').find('li').removeClass('darkText');
			} else {
				$('body').removeAttr('style');
				$('.sticky-left-nav').find('li').addClass('darkText');
			}
		}
	}

	/* This part causes smooth scrolling on nav click */
	$("nav.sticky-left-nav a").click(function (evn) {
		evn.preventDefault();
		if ($(this).closest('li').hasClass('no-scrollify') && (!$(this).closest('li').siblings('.nav-active').hasClass('no-scrollify'))) {
			$.scrollify.instantMove('#5');
			$('html, body').animate({
				scrollTop: $($(this).attr('href')).offset().top
			}, 300);
		} else {
			$('html, body').animate({
				scrollTop: $($(this).attr('href')).offset().top
			}, 300);
		}
		if ($($(this).attr('href')).hasClass('scroll-section')) {
			//$('.scroll-section').removeClass('fromUp');
			//$($(this).attr('href')).addClass('fromUp');
		}
	});

	/* left navigation hover effects */
	$(document).on('mouseenter', '.sticky-left-nav li', function () {
		if (!$(this).hasClass('mb5')) {
			$(this).addClass('nav-active');
			//$(this).toggleClass('');
		}
	});
	$(document).on('mouseleave', '.sticky-left-nav li', function () {
		if (!$(this).hasClass('mb5')) {
			$(this).removeClass('nav-active');
		}
	});

	/* scroll down click event */
	$(document).on('click', '.a-scroll-down', function (evn) {
		evn.preventDefault();
		$.scrollify.next();
	});

	/*Index End*/

	/* enable tickering based on screenwidth*/
	function enableTickering() {
		if ($(".blinkTxt").length) {
			$(".blinkTxt").typed({
				strings: ["^200 your next"],
				typeSpeed: 30,
				startDelay: 1500,
				backSpeed: 20,
				contentType: 'html',
				cursorChar: "|",
				loop: false,
				preStringTyped: function (curString) {
					var lengthOfArray = this.strings.length - 1;
					if (curString === lengthOfArray) {
						setTimeout(function () {
							$('.typed-cursor').fadeOut();
						}, 500);
					}
				}
			});
		}
	}

	/* check footer's offset value and change left-navigation's position */
	function checkFooterOffset() {
		if ($('.sticky-left-nav').offset().top + $('.sticky-left-nav').height() >= $('footer').offset().top - 10) {
			$('.sticky-left-nav').addClass('bottom-menu').removeClass('top-menu');
		}
		if ($(document).scrollTop() + window.innerHeight < $('footer').offset().top) {
			$('.sticky-left-nav').addClass('top-menu').removeClass('bottom-menu');
		}
	}

	/* ------------- SCROLL TO TARGET SECTION ---------------*/
	$(".scrollto-target").click(function (e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $($(this).attr('href')).offset().top - 20
		}, 700);
	});

	/* ------------- WAYPOINT PART ---------------*/
	//change menu icon background and logo color
	if ($("#discover").length) {
		var waypoint = new Waypoint({
			element: document.getElementById('discover'),
			handler: function () {
				$('.menu-bg').toggleClass('reverseMenu');
				$('#logo').attr('fill', function (index, attr) {
					return attr === '#007cc3' ? '#fff' : '#007cc3';
				});
				$('.burger > .icon-bar1').toggleClass('icon-bar11');
				$('.burger > .icon-bar2').toggleClass('icon-bar21');
				$('.burger > .icon-bar3').toggleClass('icon-bar31');
				$("header .navbar-header").toggleClass('hidden-xs hidden-sm hidden-md hidden-lg');
				//$("header .container > .header,.container > .header-menu").toggleClass('hidden-xs hidden-sm hidden-md hidden-lg');
				$("header .container").toggleClass('mt15');
			}
		});
	}

	/*if ($("#discover").length) {
		var waypoint1 = new Waypoint({
			element: document.getElementById('discover'),
			handler: function () {
				
			}
		});
	}*/

	/* ------------- SCROLL UP FUNCTION HOME Pages ---------------*/
	$(window).scroll(function () {
		if ($(this).scrollTop() !== 0) {
			$('.scroll-up').fadeIn(700);
			$('.header-menu').fadeOut(700);
			//$('.navbar-brand').fadeOut(700);

		} else {
			$('.scroll-up').fadeOut(700);
			$('.header-menu').fadeIn(700);
			owl.trigger('refresh.owl.carousel');
			//$('.navbar-brand').fadeIn(700);
		}

		/* ------------- Scroll ProgressBar indicator ---------------*/
		if (pageUrl !== 'home') {
			var _document_height = $(document).height(),
				_window_height = $(window).height(),
				_scroll_top = $(window).scrollTop(),
				_w = 0;
			_w = _scroll_top * 100 / (_document_height - _window_height);
			$('.progressbar').find('> div').width(_w + '%');
		}
	});

	/* ------------- Scroll up arrow visibile On load---------------*/
	if ($(this).scrollTop() !== 0) {
		$('.scroll-up').fadeIn(700);
	} else {
		$('.scroll-up').fadeOut(700);
	}

	/* ------------- SCROLL UP FUNCTION ---------------*/
	$('.scroll-up').click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 700);
	});

	/* ------------- Burger Menu ---------------*/
	$(document).on("click", ".burger", function () {

		if (!$(this).hasClass('open')) {
			//$('body').css('overflow-y', 'hidden');
			$('html, body').addClass('hidden-scroll');
			$('.listmenu').css('z-index', '2');
			$('.progressbar,.hero-list1').css('display', 'none');
			$('li.slideout').removeClass('slideout');
			$('.search-icon').css('z-index', '0');
			openMenu();
		} else {
			//$('body').css('overflow-y', 'scroll');
			$('html, body').removeClass('hidden-scroll');
			$('.listmenu').css('z-index', '9999');
			$('.progressbar,.hero-list1').css('display', 'block');
			$('ul.social-share >li.social-share-icon').addClass('slideout');
			$('.search-icon').css('z-index', '1');
			$('.fix-menu').removeClass('opacity-zero');
			$('.circle').removeClass('bg-trans');
			closeMenu();
		}
	});

	/* ------------- Social Share ---------------*/
	$(document).on("click", ".trigger-share", function () {
		$('ul.social-share > li').toggleClass('slideout social-share-icon');
	});
	$(document).on("click", ".trigger-share-pr", function () {
		$(this).closest(".social-tag").find(".social-share-pr > li").toggleClass('slideout social-share-icon');
	});

	/* ------------- Menu Hover sub menu ---------------*/
	$(document).on("mouseover", ".burger", function () {
		$('.discover-menu').addClass('fadeOutRightBig').removeClass('fadeInLeftBig').css('display', 'none');
		$('.experience-menu').addClass('fadeOutRightBig').removeClass('fadeInLeftBig').css('display', 'none');
		$('.apply-menu').addClass('fadeOutRightBig').removeClass('fadeInLeftBig').css('display', 'none');
	});

	$(document).on("mouseover", ".hover-menu-hide", function () {
		$('.discover-menu').addClass('fadeOutRightBig').removeClass('fadeInLeftBig').css('display', 'none');
		$('.experience-menu').addClass('fadeOutRightBig').removeClass('fadeInLeftBig').css('display', 'none');
		$('.apply-menu').addClass('fadeOutRightBig').removeClass('fadeInLeftBig').css('display', 'none');
		$('.fix-menu').addClass('opacity-zero');
		$('.circle').addClass('bg-trans');
	});
	$(document).on("mouseleave", ".hover-menu-hide", function () {
		$('.fix-menu').removeClass('opacity-zero');
		$('.circle').removeClass('bg-trans');
	});

	if ($(window).width() < 1025) {
		$(document).on('click', '.visible1024-cross', function () {
			$('.discover-menu').addClass('fadeOutRightBig').removeClass('fadeInLeftBig').css('display', 'none');
			$('.experience-menu').addClass('fadeOutRightBig').removeClass('fadeInLeftBig').css('display', 'none');
			$('.apply-menu').addClass('fadeOutRightBig').removeClass('fadeInLeftBig').css('display', 'none');
		});
		// Discover
		$(document).on('click', '.discover', function () {
			$('.apply-menu').addClass('fadeOutRightBig').removeClass('fadeInLeftBig').css('display', 'none');
			$('.discover-menu').addClass('fadeInLeftBig').removeClass('fadeOutRightBig').css('display', 'block');
			$('.experience-menu').addClass('fadeOutRightBig').removeClass('fadeInLeftBig').css('display', 'none');
		});
		// Experience
		$(document).on('click', '.experience', function () {
			$('.apply-menu').addClass('fadeOutRightBig').removeClass('fadeInLeftBig').css('display', 'none');
			$('.discover-menu').addClass('fadeOutRightBig').removeClass('fadeInLeftBig').css('display', 'none');
			$('.experience-menu').addClass('fadeInLeftBig').removeClass('fadeOutRightBig').css('display', 'block');
		});
		// Apply
		$(document).on('click', '.apply', function () {
			$('.discover-menu').addClass('fadeOutRightBig').removeClass('fadeInLeftBig').css('display', 'none');
			$('.apply-menu').addClass('fadeInLeftBig').removeClass('fadeOutRightBig').css('display', 'block');
			$('.experience-menu').addClass('fadeOutRightBig').removeClass('fadeInLeftBig').css('display', 'none');
		});
	} else {
		// Discover
		$(document).on("mouseover", ".discover", function () {
			$('.discover-menu').addClass('fadeInLeftBig').removeClass('fadeOutRightBig').css('display', 'block');
			$('.experience-menu').addClass('fadeOutRightBig').removeClass('fadeInLeftBig').css('display', 'none');
			$('.apply-menu').addClass('fadeOutRightBig').removeClass('fadeInLeftBig').css('display', 'none');
		});
		$(document).on("mouseleave", ".discover", function () {
			//$('.discover-menu').addClass('fadeInLeftBig').removeClass('fadeOutRightBig').css('display', 'none');
			//$('.experience-menu').addClass('fadeOutRightBig').removeClass('fadeInLeftBig').css('display', 'none');
			//$('.apply-menu').addClass('fadeOutRightBig').removeClass('fadeInLeftBig').css('display', 'none');
			$('.fix-menu').removeClass('opacity-zero');
			$('.circle').removeClass('bg-trans');
		});
		// Experience
		$(document).on("mouseover", ".experience", function () {
			$('.discover-menu').addClass('fadeOutRightBig').removeClass('fadeInLeftBig').css('display', 'none');
			$('.apply-menu').addClass('fadeOutRightBig').removeClass('fadeInLeftBig').css('display', 'none');
			$('.experience-menu').addClass('fadeInLeftBig').removeClass('fadeOutRightBig').css('display', 'block');
		});
		// Apply
		$(document).on("mouseover", ".apply", function () {
			$('.discover-menu').addClass('fadeOutRightBig').removeClass('fadeInLeftBig').css('display', 'none');
			$('.apply-menu').addClass('fadeInLeftBig').removeClass('fadeOutRightBig').css('display', 'block');
			$('.experience-menu').addClass('fadeOutRightBig').removeClass('fadeInLeftBig').css('display', 'none');
		});
	}

	function openMenu() {
		$('.circle').addClass('expand');
		$('.burger').addClass('open');
		$('.icon-bar1, .icon-bar2, .icon-bar3').addClass('collapse');
		$('.menu').fadeIn();

		setTimeout(function () {
			$('.icon-bar2').hide();
			$('.icon-bar1').addClass('rotate30');
			$('.icon-bar3').addClass('rotate150');
		}, 70);
		setTimeout(function () {
			$('.icon-bar1').addClass('rotate45');
			$('.icon-bar3').addClass('rotate135');
		}, 120);
	}

	function closeMenu() {
		$('.burger').removeClass('open');
		$('.icon-bar1').removeClass('rotate45').addClass('rotate30');
		$('.icon-bar3').removeClass('rotate135').addClass('rotate150');
		$('.circle').removeClass('expand');
		$('.menu').fadeOut();

		setTimeout(function () {
			$('.icon-bar1').removeClass('rotate30');
			$('.icon-bar3').removeClass('rotate150');
		}, 50);
		setTimeout(function () {
			$('.icon-bar2').show();
			$('.icon-bar1, .icon-bar2, .icon-bar3').removeClass('collapse');
		}, 70);
	}

	/* ------------- Mega Dropdown animation ---------------*/
	$(document).on("mouseenter", ".dropdown", function () {
		$('.dropdown-menu', this).not('.in .dropdown-menu').stop(true, true).fadeIn("800");
		$(this).toggleClass('open');
		$('ul.hidden-list').css('opacity', '0');
	});
	$(document).on("mouseleave", ".dropdown", function () {
		$('.dropdown-menu', this).not('.in .dropdown-menu').stop(true, true).hide();
		$(this).toggleClass('open');
		$('ul.hidden-list').css('opacity', '1');
	});
	/* ------------- on Click BreadCrumb link ---------------*/
	$(document).on('click', '.hero-list > ol > li.mega-dropdown', function (e) {
		e.stopPropagation();
	});

	/* ------------- Tag Part ---------------*/
	$(document).on('click', '.tag-icon', function () {
		$(this).closest(".relative").find(".tag-postion").fadeIn();
	});
	$(document).on('click', '.tag-close', function () {
		$(this).closest(".relative").find(".tag-postion").fadeOut();
	});

	/* ------------- Country Selection Part ---------------*/
	$(document).on("click", ".select-country, .option-country > ul > li", function () {
		$(".option-country").toggleClass("open-country");
		if ($(".option-country").hasClass("open-country")) {
			$(".down-arrow").addClass("up-arrow").removeClass("down-arrow");
		} else {
			$(".up-arrow").addClass("down-arrow").removeClass("up-arrow");
		}
	});

	$(document).on('click', 'body', function (e) {
		if (!$(e.target).is('.select-country > a')) {
			$('.option-country.open-country').removeClass('open-country');
			$(".up-arrow").addClass("down-arrow").removeClass("up-arrow");
		}
	});

	/* ------------- Tab Accordion Product landing ---------------*/
	if ($("#verticalTab").length) {
		$('#verticalTab').easyResponsiveTabs({
			type: 'vertical',
			width: 'auto',
			fit: true
		});
	}

	//END
});